var Paginate = (function(){

  var set = {
    list:{}
  }

  set.create = function(o){
    if(typeof set.list[o.name]=='undefined'){
      set.list[o.name] = {
        n:0,
        controller:o.name+'/paginate'
      }
      set.list[o.name].template = Handlebars.compile( $('.paginate-item-'+o.name).html() )
      set.list[o.name].box = $( '.paginate-box-'+o.name )
      if(typeof o.controller!='undefined') set.list[o.name].controller = o.controller
      set.list[o.name].callback = typeof o.fn != 'undefined' ? o.fn : function(){ }
      set.bindMoreBtn(o.name)
    }
    set.list[o.name].limit = typeof o.limit != 'undefined' ? o.limit : 20
    if(typeof o.init != 'undefined') set.getData(o.name)
  }

  set.bindMoreBtn = function(n){
    $('.paginate-btn-'+n).click(function(){
      Paginate.getData(n)
    })
  }

  set.getData = function(n){
    Main.req({
      url:Main.site+set.list[n].controller,
      data:{n:set.list[n].n, limit: set.list[n].limit},
      success: function(d){
        if(d.length==0 || d.length < set.list[n].limit)
          $('.paginate-btn-'+n).remove()
        set.renderData(n,d)
        set.list[n].n++
      }
    })
  }

  /* render of list returned data from server */
  set.renderData = function(n,d){
    for(var i=0,c=d.length; i<c; i++){
      set.renderItem(n,d[i])
    }
    set.list[n].callback( d )
  }

  /* render item */
  set.renderItem = function(n,el){
    set.list[n].box.append( set.list[n].template( el ) )
  }

  return set

})()
