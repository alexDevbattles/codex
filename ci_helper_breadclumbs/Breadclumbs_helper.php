<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter BreadClumbs Helper
 *
 * @package		  CodeIgniter
 * @subpackage	Helpers
 * @category	  Helpers
 * @author		  Alex Berezhnyk
 */

if ( ! function_exists('breadclumbs')){
	/**
	 * Create breadclumbs
	 */
	function breadclumbs($id = false){

		// structure of workflow
		$structure = [
			'direction'=>0,
			'category'=>1,
			'subcategory'=>2,
			'item'=>3,
			'link'=>4
		];
		// data for queries
		$map = [
			[
				'type'=>'direction',
				'table'=>'cat_direction',
				'filter_field_name'=>'id'
			],
			[
				'type'=>'category',
				'table'=>'cat_list',
				'filter_field_name'=>'direction_id'
			],
			[
				'type'=>'subcategory',
				'table'=>'cat_subcategory',
				'filter_field_name'=>'cat_id'
			],
			[
				'type'=>'item',
				'table'=>'cat_item',
				'filter_field_name'=>'subcat_id'
			],
			[
				'type'=>'item',
				'table'=>'cat_link',
				'filter_field_name'=>'item_id'
			]
		];

		// get CodeIgniter
		$CI = get_instance();
		// result array
		$data = [];

		$num = $structure[ $CI->router->class ] - 1;
		while($num >= 0){
			if($id) $CI->db->where('id', $id);
			$data[ $map[$num]['type'] ] = $CI->db->get( $map[$num]['table'] )->row();
			if(isset($data[ $map[$num]['type'] ]->id)) $id = $data[ $map[$num]['type'] ]->{$map[$num]['filter_field_name']};
			$data[ $map[$num]['type'] ]->type = $map[$num+1]['type'];
			$num--;
		}

		echo '<ol class="breadcrumb">';
		$str = '';
		foreach($data as $k=>$v){
			$str = '<li><a href="../../'.$v->type.'/index/'.$v->id.'">'.$v->name.'</a></li>'.$str;
		}
		$str = '<li><a href="../../direction/index">Workflow</a></li>'.$str;
		echo $str;
		echo '</ol>';
	}
}
