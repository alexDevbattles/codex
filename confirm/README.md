Devbattles confirm modal-based App
====================

## What is this

This is a part of DevBattles code - when user sign-up using Facebook we must ask him for accept "public_action" permission on our website (this is new FB Web-based App validation rule).
