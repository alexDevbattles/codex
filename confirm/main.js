/* confirm widget controller
  use ReminderView module
  example of call:
  Main.install('elements/confirm/main',1)
  Confirm.show('Facebook')
*/

var Confirm = (function(){

  var set = {
    dir:'../system/services/elements/confirm/',
    initFlag:0
  }

  // render or show existing confirm element
  set.show = function( name ){

    // get CSS
    if(!set.initFlag){
      Main.getCSS(set.dir+'style')
      set.initFlag = 1
    }
    // if 1st App call - render it
    if(typeof set[name] == 'undefined'){
      // get lang
      $.getJSON(set.dir+'storage/'+name+'/lang/'+basics.lang+'.json',function(d){
        // render
        Main.install(set.dir+'storage/'+name+'/main.js')
        set[name].lang = d
        set.render( name )
      })
    } else set[name].show()
  }

  set.render = function(name){
    // create modal
    var modalName = 'confirm'+name
    if(typeof ReminderView == 'undefined')
      Main.install('reminder/view',1)

    ReminderView.create({ type:'modal', name: modalName, size:'xs', callback: function(){

      ReminderView.list[modalName].elem.addClass('confirmBox')

      // remove close icon
      ReminderView.list[modalName].dom.close.remove()
      // title
      if(typeof set[name].lang.title !='undefined')
        ReminderView.list[modalName].dom.title.text( set[name].lang.title )
      else
        ReminderView.list[modalName].dom.title.remove()

      // text
      if(typeof set[name].lang.text !='undefined')
        ReminderView.list[modalName].dom.body.text( set[name].lang.text )

      // add buttons to footer
      var buttons = "";
      // left btn
      if(typeof set[name].lang.btnLeft!='undefined')
        buttons+='<button type="button" class="btn btn-white btn-confirm-left" data-dismiss="modal"><i class="fa fa-close"></i> '+set[name].lang.btnLeft+'</button>'
      // right btn
      if(typeof set[name].lang.btnRight!='undefined')
        buttons+='<button type="button" class="btn btn-white btn-confirm-right" data-dismiss="modal"><i class="fa fa-check"></i> '+set[name].lang.btnRight+'</button>'

      ReminderView.list[modalName].dom.footer
        .html(buttons)
        .attr('style','display:block; width:200px; margin:0 auto;')

      set[name].show( 1 )

    } })

  }

  return set

})()
