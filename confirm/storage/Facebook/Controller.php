<?php

/* AJAX server for devbattles confirm Facebook popup */

class FacebookController extends ajax {

  public $ajaxConf = [
    'change'=>['status']
  ];

  public function __construct(){
    parent::__construct();
    if(!$this->uid) exit;
    if($this->isAjax())
      $this->parseAjax();
  }

  /* change publish status for FB token */
  public function change(){

    $status = 0; # response data

    if($this->d['status'] == 'yes'){

      # get user tokens
      $token = $this->getData('user_token',['id','data'],['user_id'=>$this->uid],[],1);
      if(isset($token[0]['id'])){
        $token[0]['data'] = json_decode($token[0]['data']);
        # if fb token fount - add fbPublish flag to tokenList
        if(isset($token[0]['data']->facebook) && !isset($token[0]['data']->facebookPublish)){
          $token[0]['data']->facebookPublish = 1;
          $this->updateData('user_token',['id'=>$token[0]['id']], ['data'=>json_encode($token[0]['data'])], 1);

          $status = 1;

          # setup authThanks Facebook post for publication
          $id = $this->insertData('public_actions',[
            'user_id'=>$this->uid,
            'type'=>"facebook",
            'item'=>'auth_thanks',
            'date'=>date('Y-m-d H:i:s')
          ]);
        }
      }
    }

    # delete user_transport_data item
    unset( $_SESSION['user_transport_data']['confirm'] );

    echo json_encode(['status'=>$status]);
  }

}

?>
