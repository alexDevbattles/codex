/* facebook Auth */
Confirm.Facebook = (function(){

  var set = {
    statusCompleteFlag:0
  }

  set.show = function(n){
    n = typeof n=='undefined' ? 0 : 1
    // if first init - add events
    if(n) set.bindEvents()
    ReminderView.list.confirmFacebook.dom.footer.find('.btn-confirm-right').find('i').attr('class','icon-facebook')
    ReminderView.show('confirmFacebook')
  }

  set.statusNo = function(){
    if(!set.statusCompleteFlag){
      Main.req({
        url: '../system/services/elements/confirm/storage/Facebook/Controller.php',
        data: {status:'no',get:'change'}
      })
      set.statusCompleteFlag = 1
    }
  }

  set.statusYes = function(){
    if(!set.statusCompleteFlag){
      Main.req({
        url: '../system/services/elements/confirm/storage/Facebook/Controller.php',
        data: {status:'yes',get:'change'},
        success:function(d){
          // if accepted - say thanks for client
          if(typeof d.response != 'undefined'){
            ReminderView.list.confirmFacebook.dom.title.text(set.lang.thanks)
            ReminderView.list.confirmFacebook.dom.body.text(set.lang.thanksText)
            ReminderView.show('confirmFacebook')
          }
        }
      })
      set.statusCompleteFlag = 1
    }
  }

  set.bindEvents = function(){
    // decline
    ReminderView.list.confirmFacebook.dom.footer.find('.btn-confirm-left').bind('click', set.statusNo)
    // accept
    ReminderView.list.confirmFacebook.dom.footer.find('.btn-confirm-right').bind('click', set.statusYes)
  }

  return set

})()
