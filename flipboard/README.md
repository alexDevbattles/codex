FlipBoard self-xss parser
====================

## What is this

This is self-xss JavaScript Module-based scrapping code, which consist of set of 5th modules and do data scrapping from FlipBoard.com website.

Data store inside the browser localStorage and can be exported to JSON files.

*note: to use more than 10M of localStorage - increase it on Firefox by using about:config window
