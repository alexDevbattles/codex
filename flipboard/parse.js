function log(e){console.log(e)}

/* main module */
var Flip = (function(){
  var set = {}
  set.init = function(){
    // set variables into storage
    var a = ['catList','topicList','magazineList','userList','workers','usName','followList']
    for(var i=0,c=a.length;i<c;i++){
      if(localStorage.getItem(a[i])==null)
        localStorage.setItem(a[i],"{}")
    }
  }

  set.init()

  /* localStorage helpers: n -name, f - 1 will parse JSON */
  set.get = function(n,f){
    f = typeof f == 'undefined' ? 0 : 1
    var o = localStorage.getItem(n) == null ? {} : localStorage.getItem(n)
    return f ? o : JSON.parse(o)
  }

  set.set = function(n,v,f){
    f = typeof f == 'undefined' ? 0 : 1
    localStorage.setItem(n, f ? v : JSON.stringify(v))
  }

  // helper aliases
  set.log = function(n){ Flip.Helpers.log(n) }
  set.download = function(n){ Flip.Helpers.download(n) }
  set.getDiff = function(n){ Flip.Helpers.getDiff(n) }
  return set

})()

/* Category CRUD */
Flip.Cat = (function(){

  var set = {
    list:{}
  }

  set.init = function(){
    if(localStorage.getItem('catList')!=null)
      set.list = JSON.parse(localStorage.getItem('catList'))
    else {
      set.list = {}
      localStorage.setItem('catList',{})
    }
  }

  /* n - name, f - 1 run Worker for this cat */
  set.add = function(n,f){

    f = typeof f == 'undefined' ? 0 : 1
    var topics = Flip.get('topicList')

    for(var i=0,c=n.length;i<c;i++){
      if(typeof topics[n[i]]=='undefined'){
        set.list[n[i]] = {}
        topics[n[i]] = n[i]
        if(f){
          Flip.Workers.create({
            name:n[i],
            data:{
              cat:n[i],
              job:'Topic/parse',
              recursion:3
            }
          })
          topics[n[i]] = 'current'
        }
      }
    }
    Flip.set('catList',set.list)
    Flip.set('topicList',topics)
  }

  set.getCat = function(){
    /* method not finished */
    var curr = localStorage.getItem('currCat'),
      list = localStorage.getItem('catList')
  }

  return set

})()

/* Workers module */
Flip.Workers = (function(){
  var set = {
    list:{}
  }

  /* create new task */
  set.create = function(o){
    var list = Flip.get('workers')
    o = $.extend({
      name:'First',
      data:{
        cat:'default',
        job:'Topic/parse'
      }
    },o)
    log(o)
    if(typeof list[o.name]=='undefined'){
      list[o.name] = o.data
      set.list[o.name] = o.data
      Flip.set('workers',list)
      set.run(o.name)
    }
  }

  set.run = function(n){
    log(n+' started')
    var route = set.list[n].job.split('/')
    log(route)
    if(typeof Flip[route[0]] !== 'undefined')
      if(typeof Flip[route[0]][route[1]] !== 'undefined')
        set.list[n].interval = setInterval(function(){
          if(typeof set.list[n].worked == 'undefined'){
            Flip[route[0]][route[1]](n)
            set.list[n].worked = 1
          } else if(set.list[n].worked == 1){
            clearInterval(set.list[n].interval)
            set.list[n].worked=2
            set.list[n].interval = setInterval(function(){
              Flip[route[0]][route[1]](n)
            },5000)
          }
        }, set.list[n].time ? set.list[n].time : 15000)
  }

  return set

})()

/* Topic parser */
Flip.Topic = (function(){
  var set = {
    user:[],
    topic:[],
    map:{
      user:'.avatar',
      topic:'.topic-tag'
    }
  }

  set.parse = function(n){
    log('do')
    log(Flip.Workers.list[n])
    // if task new - create item
    if(typeof Flip.Workers.list[n].popup == 'undefined'){
      Flip.Workers.list[n].popup = window.open("https://flipboard.com/topic/"+Flip.Workers.list[n].cat)
      Flip.Workers.list[n].scroll = {h:0, check:0}
      return 1
    }
    // scroll page & check it end
    var size = $('.page-content',Flip.Workers.list[n].popup.document).size()
    Flip.Page.scroll(Flip.Workers.list[n].popup)
    // parse data
    setTimeout(function(){
      // if this is end of topic - make recursion to other finded topic
      if(set.checkTopicEnd(size,n)){
        set.nextTopic(n)
        return 1
      }
      // parse users
      $.each(set.map,function(k,v){
        $(v,Flip.Workers.list[n].popup.document).each(function(){
          var el = $(this).is('a') ? $(this) : $(this).closest('a')
          if(typeof el.attr('href')!='undefined' && typeof el.attr('parsed') == 'undefined' && el.attr('href').length > 0){
            log('parse: '); log(el); log(el.attr('href'))
            var t = el.attr('href').split(k=='user' ? '@' : 'topic/')[1]
            if(t!='/@profile' && t!='undefined'){
              set[k].push(t)
              $(this).attr('parsed',1)
            }
          }
        })
        set.save(k,n)
      })
    },10000)
  }

  set.checkTopicEnd = function(s,n){
    log('check end '+s+' - '+$('.page-content',Flip.Workers.list[n].popup.document).size())
    if($('.page-content',Flip.Workers.list[n].popup.document).size() == s){
      Flip.Workers.list[n].scroll.check++
      // if this 5 falls on the end of page
      if(Flip.Workers.list[n].scroll.check == 5)
        return 1
    }
  }

  set.save = function(k,n){
    log('save '+k); log(set[k])
    var list = Flip.get(k+'List')
    for(var i=0,c=set[k].length;i<c;i++){
      // if new item - add it
      if(typeof list[set[k][i]] == 'undefined'){
        if(typeof set[k][i]!='undefined' && set[k][i]!='undefined' && set[k][i]!=null){
          if(k=='user')
            list[set[k][i]] = {
              u:set[k][i],c:n
            }
          else
            list[set[k][i]] = {}
        }
      }
    }
    set[k] = []
    Flip.set(k+'List',list)
  }

  /* kill current and make next topic Worker */
  set.nextTopic = function(n){
    var topics = Flip.get('topicList')

    // complete current topic
    topics[n] = 'finished'
    clearInterval(Flip.Workers.list[n].interval)
    Flip.Workers.list[n].popup.close()
    delete Flip.Workers.list[n]
    var topics = Flip.get('topicList')
    // set next topic Worker
    var t = false
    $.each(topics,function(k,v){
      if(k != 'undefined'){
        if(v!='finished' && v!='current'){
          log(k); log(v)
          t = k
          return 1
        }
      } else delete topics[k]
    })

    Flip.Workers.create({
      name:t,
      data:{
        cat:t,
        job:'Topic/parse',
        recursion:3
      }
    })
    topics[t] = 'current'
    Flip.set('topicList',topics)
  }

  return set

})()

/* Profile parser */
Flip.User = (function(){

  var set = {
    tmp:[]
  }

  set.init = function(){
    var t = set.getNextUser()
    log('user '+t)
    Flip.Workers.create({
      name:t,
      data:{
        user:t,
        job:'User/getFollowers'
      }
    })
  }

  set.getNextUser = function(){
    var list = Flip.get('userList')
    var job = Flip.get('workers')
    var n = false
    $.each(list,function(k,v){
      if(v!='undefined' && v!='finished' && v!='current' && typeof job[k] == 'undefined')
        n = k
    })
    if(n && n.length>0){
      Flip.set('userList',list)
      return n
    }
  }

  set.getFollowers = function(n){

    // if this task is ended (non-suspected call)
    if(typeof Flip.Workers.list[n].busy!='undefined'){ log('user wait for interval'); return 1 }
    // if task new - create item
    if(typeof Flip.Workers.list[n].popup == 'undefined'){
      Flip.Workers.list[n].popup = window.open("https://flipboard.com/@"+Flip.Workers.list[n].user)
      return false
    }
    // check page loading
    if($('body',Flip.Workers.list[n].popup.document).text() == "Too busy"){
      Flip.Workers.list[n].popup.document.location.reload()
    }
    if(!$(".header-title",Flip.Workers.list[n].popup.document).size()) return false
    // start parse if all is ok
    Flip.Workers.list[n].busy = 1
    $("span:contains('Followers')",Flip.Workers.list[n].popup.document).click()
    var list = Flip.get('userList')
    setTimeout(function(){
      $('div[class="avatar"]',Flip.Workers.list[n].popup.document).parent('a').each(function(){
        if(typeof $(this).attr('href') != 'undefined' && $(this).attr('href')!='/profile'){
          set.tmp.push($(this).attr('href').split('@')[1])
        }
      })
      if(set.tmp.length>0){
        log('save profile parser users')
        var saved = []
        for(var i=0,c=set.tmp.length;i<c;i++){
          // if new item - add it
          if(typeof list[set.tmp[i]] == 'undefined'){
            if(typeof set.tmp[i]!='undefined' && set.tmp[i]!='undefined' && set.tmp[i]!=null){
              list[set.tmp[i]] = {}
              saved.push(set.tmp[i])
            }
          }
        }
        log(saved)
        set.tmp = []
      }
      // remove current and start new User check
      list[n] = 'finished'
      log('fine of '+n)
      clearInterval(Flip.Workers.list[n].interval)
      Flip.Workers.list[n].popup.close()
      delete Flip.Workers.list[n]
      Flip.set('userList',list)
      setTimeout(function(){
        Flip.User.init()
        console.clear()
      },10000)
    },10000)
  }

  return set
})()

/* Get users by names */
Flip.Names = (function(){

  var set = {
    curr: null,
    i:0
  }

  // load list of names
  set.init = function(){
    if(typeof set.list == 'undefined'){
      set.list = Flip.get('usName')
      set.user = Flip.get('userList')
    }
  }
  set.init()

  set.create = function(n){
    set.init()
    if(typeof set.list[n] == 'undefined' && typeof n!='undefined' && n)
      set.list[n] = {}
    Flip.set('usName',set.list)
  }

  set.add = function(n, id){
    set.init()
    n = $.trim( n )
    if(typeof set.user[n] == 'undefined' && typeof n!='undefined' && n){
      log('add '+n)
      set.user[n] = {}
    } else log(n+' al ex')
    Flip.set('userList',set.user)
  }

  /* run parser */
  set.go = function(){
    console.clear()
    // set current item
    $.each(set.list, function(k,v){
      if(typeof v.dd == 'undefined'){
        set.curr = k; return false
      }
    })

    set.parse(set.curr)

    set.list[set.curr].dd = 1
    Flip.set('usName', set.list)

    set.i++
    time = set.i%10 == 0 ? 5000 : 500

    setTimeout(function(){ set.go() },time)
  }

  /* parse current item */
  set.parse = function(n){
    $.getJSON("api/social/sectionSearch?q="+n+"&categories=medium2&nostream=true",function(d){
      if(d){
        for(var i=0,c=d.stream.length;i<c;i++){
          if(d.stream[i].category == 'profile'){
            if(typeof d.stream[i].searchResultItems != 'undefined'){
              var tmp = d.stream[i].searchResultItems
              for(var j=0, x=tmp.length; j<x; j++){
                set.add( tmp[j].username )
              }
            }
          }
        }
      }
    })
  }

  return set

})()

/* Follows module */
Flip.Follow = (function(){

  var set = {
    list:{} // list of users
  }

  set.init = function(){
    set.list = Flip.get('followList')
  }

  set.init()

  // add new user
  set.create = function(o, f){
    f = typeof f == 'undefined' ? 0 : 1
    // set defaults
    o = $.extend({
      source : 'a_1',
      count : 0,
      limit : 10000,
      name : 'Alex',
      curr: null
    },o)

    // get Users list
    var list = Flip.get('followList')
    // if this user is new
    if( typeof list[o.name] == 'undefined' ){
      // prepare source
      list[o.name] = o
      Flip.set('followList', list)
      set.list = list
    } else log(o.name+' already exists')
    // if flag 1 - run workers for this user
    if(f)
      set.run(o.name)
  }

  // get next not followed user
  set.getNext = function( n ){
    log('get next '+n)
    log(set.list[n])
    var list = Flip.get(set.list[n].source)
    if( typeof list[ set.list[n].count+1 ] != 'undefined'){
      set.list[n].count++
      return list[ set.list[n].count ]
    } else
      return false
  }

  // run the existing user
  set.run = function( n ){
    log('run of '+n)
    var t = set.getNext( n )
    log('next is '+t)
    if(t){
      log('run next')
      Flip.Workers.create({
        name : t,
        data : {
          time : 5000,
          user : n,
          job : 'Follow/follow'
        }
      })
    } else console.error(n+' is full done')
  }

  set.follow = function(n){
    // if this task is ended (non-suspected call)
    if(typeof Flip.Workers.list[n].busy!='undefined'){ log('user wait for interval'); return 1 }
    // if task new - create item
    if(typeof Flip.Workers.list[n].popup == 'undefined'){
      Flip.Workers.list[n].popup = window.open("https://flipboard.com/@"+n)
      return false
    }
    // check page loading
    if($('body',Flip.Workers.list[n].popup.document).text() == "Too busy"){
      Flip.Workers.list[n].popup.document.location.reload()
    }
    if(!$(".follow-button",Flip.Workers.list[n].popup.document).size()){
      Flip.Workers.list[n].tw = typeof Flip.Workers.list[n].tw != 'undefined' ? Flip.Workers.list[n].tw+1 : 1
      if(Flip.Workers.list[n].tw == 4){
        set.finishTask( n )
      }
      return false
    }
    // start parse if all is ok
    Flip.Workers.list[n].busy = 1
    // click Follow button
    $(".follow-button",Flip.Workers.list[n].popup.document).click()

    set.finishTask( n )
  }

  set.finishTask = function(n){
    var w = Flip.get('workers')
    var currUser = Flip.Workers.list[n].user

    setTimeout(function(){
      // delete current task
      log('fine of '+n)
      clearInterval(Flip.Workers.list[n].interval)
      Flip.Workers.list[n].popup.close()
      delete Flip.Workers.list[n]
      delete w[n]

      log('save step')
      Flip.set('followList', Flip.Follow.list)
      Flip.set('workers', w)

      // run next
      setTimeout(function(){
        Flip.Follow.run( currUser )
        console.clear()
      },1000)
    }, 1000)
  }

  return set

})()

/* page actions */
Flip.Page = (function(){

  var set = {}

  /* scroll body: e - window link, v - val to scroll */
  set.scroll = function(e){
    e['document'].body.scrollTop = $('body',e['document'])[0].scrollHeight
  }

  /* click on user image */
  set.goUser = function(e){ }

  /* click followers */
  set.openFollow = function(e){
    $('.follow-text:contains("Follow")', e.document)[1].click()
  }

  return set

})()

/* Helpers (log, download ...) */
Flip.Helpers = (function(){

  var set = {}

  /* separate all data into partials
    n - partials prefix
    l - partial segment length
  */
  set.separate = function(n,l){
    var list = Flip.get('userList')
    var tmp = [], i = 0, c = 1
    $.each(list,function(k,v){
      // if segment is fill
      if(i == l){
        // create serment item
        localStorage.setItem( n+'_'+c, JSON.stringify(tmp) )
        // reset of vars
        tmp = []; i = 0; c++
      }
      tmp[i] = k
      i++
    })
  }

  /* log some item length */
  set.log = function(n){
    var i=0, list=Flip.get(n)
    $.each(list,function(k){ i++ })
    log(n+' ln '+i)
    return i
  }

  set.download = function(n){
    var d = set['download_'+n]()

    $('body').append('<button id="create">Create file</button> <a download="'+n+'_'+set.l+'.txt" style="display:block;opacity:1.0;" id="downloadlink">Download</a>')

    var textFile = null,
    makeTextFile = function(text){
      var data = new Blob([text], {type: 'text/plain'})
      if (textFile !== null){
        window.URL.revokeObjectURL(textFile)
      }
      textFile = window.URL.createObjectURL(data)
      return textFile
    }

    var create = document.getElementById('create'),
      textbox = document.getElementById('textbox')

    create.addEventListener('click',function(){
      var link = document.getElementById('downloadlink')
      link.href = makeTextFile(d)
    }, false)
    $('#create').click()
    $('#downloadlink')[0].click()
  }

  set.download_userList = function(){
    var tmp = []
    set.l = 0
    var list = Flip.get('userList')
    $.each(list,function(k,v){
      tmp.push(k)
      set.l++
    })
    return tmp.join(',')
  }

  set.download_diff = function(){
    return set.diffList.join(',')
  }

  set.getDiff = function(d,t){
    t = typeof t == 'undefined' ? 'userList' : t
    var list = Flip.get(t), res = []
    $.each(list,function(k,v){
      var f = false
      for(var i=0,c=d.length; i<c; i++){
        if(k == d[i]) f = 1
      }
      if(!f) res.push("'"+k+"'")
    })
    if(res.length){
      set.diffList = res
      set.download('diff')
    }
  }

  return set

})()
